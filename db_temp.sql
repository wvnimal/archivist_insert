-- heroku psql -a closer-archivist-jenny < db_temp.sql;

-- sequence;
\qecho 'Temp Sequence';
DROP TABLE temp_sequence;
CREATE TABLE temp_sequence (
  Label varchar(255),
  Parent_Type varchar(255),
  Parent_Name varchar(255),
  Branch int,
  Position int
);
 
\COPY temp_sequence FROM 'archivist_tables/sequence.csv' DELIMITER ';' CSV HEADER;


-- statement;
\qecho 'Temp Statement';
DROP TABLE temp_statement;
CREATE TABLE temp_statement (
  Label varchar(255),
  Literal varchar(1000),
  Parent_Type varchar(255),
  Parent_Name varchar(255),
  Branch int,
  Position int
);
 
\COPY temp_statement FROM 'archivist_tables/statement.csv' DELIMITER ';' CSV HEADER;


-- question_item;
\qecho 'Temp Question Item';
DROP TABLE temp_question_item;
CREATE TABLE temp_question_item (
  Label varchar(255),
  Literal varchar(1000),
  Instructions varchar(1000),
  Response varchar(255),
  Parent_Type varchar(255),
  Parent_Name varchar(255),
  Branch int,
  Position int,
  min_responses int, 
  max_responses int
);
 
\COPY temp_question_item FROM 'archivist_tables/question_item.csv' DELIMITER ';' CSV HEADER;


-- question_grid;
\qecho 'Temp Question Grid';
DROP TABLE temp_question_grid;
CREATE TABLE temp_question_grid (
  Label varchar(255),
  Literal varchar(1000),
  Instructions varchar(1000),
  Horizontal_Codelist_Name varchar(255),
  Vertical_Codelist_Name varchar(255),
  Response_domain varchar(255),
  Parent_Type varchar(255),
  Parent_Name varchar(255),
  Branch int,
  Position int,
  Horizontal_min_responses int, 
  Horizontal_max_responses int, 
  Vertical_min_responses int, 
  Vertical_max_responses int
);
 
\COPY temp_question_grid FROM 'archivist_tables/question_grid.csv' DELIMITER ';' CSV HEADER;


-- response_domain;
\qecho 'Temp Response Domain';
DROP TABLE temp_response;
CREATE TABLE temp_response (
  Label varchar(255),                                                   
  Type varchar(255),
  Type2 varchar(255),
  Format varchar(255),
  Min FLOAT,
  Max FLOAT
);
 
\COPY temp_response FROM 'archivist_tables/response.csv' DELIMITER ';' CSV HEADER;


-- codelist;
\qecho 'Temp Code list';
DROP TABLE temp_codelist;
CREATE TABLE temp_codelist (
  Label varchar(255),
  Code_Order int,
  Code_Value int,
  Category varchar(1000)
);
 
\COPY temp_codelist FROM 'archivist_tables/codelist.csv' DELIMITER ';' CSV HEADER;


-- condition;
\qecho 'Temp Condition';
DROP TABLE temp_condition;
CREATE TABLE temp_condition (
  Label varchar(255),
  Literal varchar(1000),
  Logic varchar(1000),
  Parent_Type varchar(255),
  Parent_Name varchar(255),
  Branch int,
  Position int
);
 
\COPY temp_condition FROM 'archivist_tables/condition.csv' DELIMITER ';' CSV HEADER;


-- loop;
\qecho 'Temp Loop';
DROP TABLE temp_loop;
CREATE TABLE temp_loop (
  Label varchar(255),
  Loop_While varchar(1000),
  Start_Value int,
  End_Value int,
  Variable varchar(255),
  Parent_Type varchar(255),
  Parent_Name varchar(255),
  Branch int,
  Position int
);
 
\COPY temp_loop FROM 'archivist_tables/loop.csv' DELIMITER ';' CSV HEADER;
